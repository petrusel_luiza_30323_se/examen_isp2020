package exam.sub2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Application extends JFrame {
    JFrame frame;
    JPanel panel;

    JTextField textField;
    JTextArea textArea;
    JButton button;

    public Application(){
    frame = new JFrame();
        frame.setSize(1530, 830);
        frame.setTitle("APPLICATION");
        frame.setResizable(false);
        frame.setLayout(null);

        panel = new JPanel();
        panel.setSize(500, 530);
        panel.setBounds(150, 50, 1200, 700);
        panel.setBackground(new Color(77, 169, 255));
        panel.setLayout(null);
        panel.setVisible(true);
        frame.add(panel);

        textField = new JTextField();
        textField.setBounds(120, 120, 400, 20);
        panel.add(textField);

        textArea = new JTextArea();
        textArea.setBounds(200, 200, 400, 20);
        panel.add(textArea);


    button = new JButton("Append");
        button.setBounds(200, 150, 300, 30);
        button.setBackground(new Color(204, 230, 255));
        button.setFont(new Font("Monospaced", Font.BOLD, 20));
        button.setForeground(Color.WHITE);
        button.setFocusable(false);
        button.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == button) {
                //s <- input from textfield
                String s = textField.getText();

                //display file content
                textArea.setText(s);

            }
        }
    });
        panel.add(button);


        frame.setVisible(true);
}
}


